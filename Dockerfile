FROM python:3-alpine

ARG COMMIT_TAG
ARG COMMIT_SHA


WORKDIR /code

COPY ./requirements.txt /code
RUN pip install -r requirements.txt

RUN mkdir db
COPY . /code

ENV CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH{}

VOLUME /code/db


RUN sed -i "s/%%%BUILDTAG%%%/$COMMIT_TAG/" blog/templates/blog/base.html
RUN sed -i "s/ff9400/$COMMIT_SHA/" blog/static/css/blog.css

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000

